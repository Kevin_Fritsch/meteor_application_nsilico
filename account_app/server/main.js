import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Projects = new Mongo.Collection('projects');

Meteor.startup(() => {
  // code to run on server at startup
});
