import { Template } from 'meteor/templating';
import { Projects } from '../lib/collections.js';
import  uploadcare  from 'meteor/uploadcare:uploadcare-widget';
import './main.html';

Template.body.helpers({
  projects(){
    return Projects.find({owner:Meteor.userId()});
  }
});

Template.add_project.events({
  'submit .project_informationForm':function(){
    
      const project_title = document.getElementById('project_title').value;
      const project_description = document.getElementById('project_description').value;
      const subject_mrn = document.getElementById('subject_mrn').value;
      const project_dob = document.getElementById('dob').value;
      const pathology_number = document.getElementById('pathology_number').value;
      const project_comment = document.getElementById('comment').value;
      read_file_type = null;

      if(document.getElementById('single_end').checked){
        read_file_type = document.getElementById('single_end').value;
      }
      else if(document.getElementById('paired_end').checked){
        read_file_type = document.getElementById('paired_end').value;
      }
      project_picture = null;
      if(document.getElementById('picture2').value != ""){
        project_picture = document.getElementById('picture2').value;
      }
      Projects.insert({
        owner : Meteor.userId(),
        date_creation : new Date(),
        project_title,
        project_description,
        subject_mrn,
        project_dob,
        pathology_number,
        project_comment,
        read_file_type,
        project_picture,
      });


      location.reload();

      return false;
  }
});

Template.project_list.events({
  'click .delete-note':function(){
    if(this.owner !== Meteor.userId()){
      throw new Meteor.Error('not-authorized');
    }
    Projects.remove(this._id);
    
    return false;
  }
});




